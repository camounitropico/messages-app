import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MensajesDAO {
    public  static void crearMensajeDB(Mensajes mensaje ){
        Conexion dbConnect = new Conexion();

        try(Connection conexion = dbConnect.get_connection()) {
            PreparedStatement ps = null;
            try {
                String query = "INSERT INTO mensajes (mensaje, autor_mensaje) VALUES (?, ?)";
                ps = conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setString(2, mensaje.getAutor_mensaje());
                ps.executeUpdate();
                System.out.println("Mensaje Creado");
            }catch (SQLException ex){
                System.out.println(ex);
            }

        }catch(SQLException e){
            System.out.println(e);

        }

    }

    public static void leermensajeDB(){
        Conexion dbConnect = new Conexion();

        PreparedStatement ps = null;
        ResultSet rs = null;

        try(Connection conexion = dbConnect.get_connection()) {
            String query = "SELECT * FROM mensajes";
            ps = conexion.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()){

                /*System.out.println("ID: " + rs.getInt("id_mensaje"));
                System.out.println("Mensaje: " + rs.getString("mensaje"));
                System.out.println("Autor: " + rs.getString("autor_mensaje"));
                System.out.println("Fecha: " + rs.getDate(4));
                System.out.println("\n");*/

                ArrayList datosMensajes= new ArrayList();
                datosMensajes.add(rs.getInt(1));
                datosMensajes.add(rs.getString(2));
                datosMensajes.add(rs.getString(3));
                datosMensajes.add(rs.getString(4));
                for (int i = 0; i < datosMensajes.size(); i++) System.out.println(datosMensajes.get(i) + "\n");
            }

        }catch(SQLException e){
            System.out.println("Data no found");
            System.out.println(e);
        }

    }

    public static void borrarMensajeDB(int id_mensaje){
        Conexion dbConnect = new Conexion();

        try(Connection conexion = dbConnect.get_connection()) {
            PreparedStatement ps = null;

            try {
                String query = "DELETE FROM mensajes WHERE id_mensaje = ?";
                ps = conexion.prepareStatement(query);
                ps.setInt(1, id_mensaje);
                ps.executeUpdate();
                System.out.println("The message has been delete");
                }catch(SQLException ex){
            System.out.println("Cant delete message");
            System.out.println(ex);
            }
        }catch(SQLException e){
            System.out.println(e);
        }


    }

    public static void actaulizarMensajeDB(Mensajes mensaje){
        Conexion dbConnect = new Conexion();

        try(Connection conexion = dbConnect.get_connection()) {
            PreparedStatement ps = null;

            try {
                String query = "UPDATE mensajes SET mensaje = ? WHERE id_mensaje = ?";
                ps =conexion.prepareStatement(query);
                ps.setString(1, mensaje.getMensaje());
                ps.setInt(2, mensaje.getId_mensaje());
                ps.executeUpdate();
                System.out.println("The message is update");
            } catch (SQLException ex){
                System.out.println(ex);
            }

        }catch(SQLException e){
            System.out.println(e);
        }



    }


}
