import java.util.Scanner;

public class MensajesService {

    public static void crearMensaje(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Escribe tu mensaje");
        String mensaje = sc.nextLine();

        System.out.println("Tu nombre");
        String nombre = sc.nextLine();

        Mensajes registro = new Mensajes();
        registro.setMensaje(mensaje);
        registro.setAutor_mensaje(nombre);
        MensajesDAO.crearMensajeDB(registro);

    }

    public static void listarMensajes(){
        MensajesDAO.leermensajeDB();
    }

    public static void borrarMensaje(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Please insert ID message");
        int id_mensaje = sc.nextInt();
        MensajesDAO.borrarMensajeDB(id_mensaje);

    }

    public static void editarMensaje(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Write your new message");
        String mensaje = sc.nextLine();
        System.out.println("Please insert ID message");
        int id_mensaje = Integer.parseInt(sc.nextLine());


        Mensajes actualizacion = new Mensajes();
            actualizacion.setId_mensaje(id_mensaje);
            actualizacion.setMensaje(mensaje);
        MensajesDAO.actaulizarMensajeDB(actualizacion);
    }


}
